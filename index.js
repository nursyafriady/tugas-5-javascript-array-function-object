// Soal 1
var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarBuah.sort().forEach(function (item) {
  console.log(item);
});

// Soal 2
console.log("\n");
function introduce(data) {
  return (
    "Nama saya " +
    data.name +
    ", umur saya " +
    data.age +
    " tahun," +
    " alamat saya di " +
    data.address +
    ", dan saya punya hobby yaitu " +
    data.hobby
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

var perkenalan = introduce(data);
console.log(perkenalan); // "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Soal 3
function hitung_huruf_vokal(string) {
  var lowerCase = string.toLowerCase();
  var hasil = "";
  for (var i = 0; i < lowerCase.length; i++) {
    if (
      lowerCase[i] == "a" ||
      lowerCase[i] == "e" ||
      lowerCase[i] == "i" ||
      lowerCase[i] == "u" ||
      lowerCase[i] == "o"
    ) {
      hasil += string[i];
    }
  }
  return hasil.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad"); // hasilLowerCase -> uaa
var hitung_2 = hitung_huruf_vokal("Iqbal"); // hasilLowerCase-> ia
console.log(hitung_1, hitung_2); // 3 2

// Soal 4
function hitung(angka) {
  var minus = 2;
  var tambah = 0;

  if (angka <= 1) {
    for (var i = 0; i <= angka; i++) {
      var hasil = angka - minus;
      minus--;
    }
    return hasil;
  }
  if (angka > 1) {
    for (var i = 2; i <= angka; i++) {
      var hasil = angka + tambah;
      tambah++;
    }
    return hasil;
  }
}

console.log(hitung(0)); //  -2 = 0 - 2
console.log(hitung(1)); //   0 = 1 - 1
console.log(hitung(2)); //   2 = 2 + 0
console.log(hitung(3)); //   4 = 3 + 1
console.log(hitung(4)); //   6 = 4 + 2
console.log(hitung(5)); //   8 = 5 + 3
console.log(hitung(6)); //  10 = 6 + 4
console.log(hitung(7)); //  12 = 7 + 5
console.log(hitung(8)); //  14 = 8 + 6
console.log(hitung(9)); //  16 = 9 + 7
console.log(hitung(10)); // 18 = 10 + 8
